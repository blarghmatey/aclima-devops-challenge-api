import hug

import hello_world


def test_hello_world():
    assert hello_world.hello_world() == {'message': 'Hello World!'}


def test_root_path():
    response = hug.test.get(hello_world, '/')
    assert response.status == '200 OK'
    assert response.data == {"message": "Hello World!"}
    assert response.content_type == 'application/json'
